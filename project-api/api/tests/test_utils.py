# External Packages
import datetime
from django.test import TestCase

# Internal Packages
from api.utils import *
from api.tests.freelancer import DATA


class TestUtilsFunctions(TestCase):
    def setUp(self):
        self.professional_experiences = DATA["freelance"]["professionalExperiences"]
        self.end_date = datetime.datetime(2018, 5, 1, 0, 0)
        self.start_date = datetime.datetime(2016, 1, 1, 0, 0)
        self.new_array = create_data_structure(self.professional_experiences)
        self.skill_object = create_skill_object(self.new_array)
            
    def test_create_data_structure(self):
        new_data_structure = create_data_structure(self.professional_experiences)
        self.assertEqual(type(new_data_structure), list)
        self.assertIn('start_date', new_data_structure[0].keys())
        self.assertIn('end_date', new_data_structure[0].keys())
        self.assertIn('total_months', new_data_structure[0].keys())
        self.assertIn('skills', new_data_structure[0].keys())

    def test_calculate_total_months(self):
        total_months = calculate_total_months(self.end_date, self.start_date)
        self.assertEqual(total_months, 28)

    def test_check_month_overlaped(self):
        data = check_month_overlaped(self.new_array)
        self.assertIn('overlaped_months', data[0].keys())

    def test_check_overlaped_skills(self):
        months_with_overlaped = check_month_overlaped(self.new_array)
        data = check_overlaped_skills(months_with_overlaped)
        self.assertEqual(data, ['Javascript', 'Java'])

    def test_create_skill_object(self):
        array_of_skills = create_skill_object(self.new_array)
        self.assertIn('React', array_of_skills[0].keys())

    def test_generate_computed_skills(self):
        generated_skills = generate_computed_skills(self.new_array, self.skill_object)
        self.assertIn('durationInMonths', generated_skills[0].keys())
