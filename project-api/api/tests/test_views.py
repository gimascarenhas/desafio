# External Packages
from rest_framework.test import APITestCase
from django.test import TestCase

# Internal Packages
from api.views import FreelancerViewSet
from api.tests.freelancer import DATA


class FreelancerViewSetTestCase(APITestCase):
    def setUp(self):
        self.url = '/freelancer/'

    def test_with_no_data(self):
        response = self.client.post(self.url, data={}, format='json')
        self.assertEqual(response.status_code, 422)

    def test_with_data(self):
        response = self.client.post(self.url, data=DATA, format='json')
        self.assertEqual(response.status_code, 200)
        
