""" 
utils.py
This file contains the functions to clean and structure the data.

Created by: Giovanna Mascarenhas.
"""

# External Packages
import datetime


def calculate_total_months(end_date: datetime, start_date: datetime) -> int:
    """This function calculates the total of months for all experiences object."""
    total_months = end_date.month - start_date.month + 12 * (end_date.year - start_date.year)
    return total_months


def create_data_structure(professionalExperiences: list) -> list:
    """ 
    This function organizes the data into dictionaries with 
    end date, start date, total months and skills.
    """
    new_array = []
    objects = {}
    for expe in professionalExperiences:
        objects.clear()
        start_date = datetime.datetime.strptime(expe["startDate"][0:10], '%Y-%m-%d')
        objects['start_date'] = start_date
        
        end_date = datetime.datetime.strptime(expe["endDate"][0:10], '%Y-%m-%d')
        objects['end_date'] = end_date
        
        # Calculate the total of months
        total_months = calculate_total_months(end_date, start_date)
        objects['total_months'] = total_months
        
        skills = expe["skills"]
        objects['skills'] = skills
        new_array.append(objects.copy())
    return new_array
    

def check_month_overlaped(new_array: list) -> list:
    """This function checks the overlaped months. 
    """
    for item in range(len(new_array)-1):     
        overlaped_months = 0
        if new_array[item]['end_date'] >= new_array[item+1]['start_date'] and new_array[item]['start_date'] <= new_array[item+1]['end_date']: 
            overlaped_months = (new_array[item+1]['end_date'].year - new_array[item]['start_date'].year) * 12 + (new_array[item+1]['end_date'].month - new_array[item]['start_date'].month)  
            new_array[item]['overlaped_months'] = overlaped_months
        else:
            new_array[item]['overlaped_months'] = overlaped_months
    return new_array


def check_overlaped_skills(array: list) -> list:
    """This function checks the overlaped skills."""
    overlaped_skills = []
    for item in array:
        if 'overlaped_months' in item.keys():
            overlaped_skills = [skill['name'] for skill in item['skills']]
    return overlaped_skills


def create_skill_object(array: list) -> list:
    """
    This function creates a dictionary with all the 
    skills.
    """
    skill_data = {}
    data = []
    for item in array:
        for skill in item['skills']:
            name = skill['name']
            skill_data[name] = []
            if skill_data not in data:
                data.append(skill_data)
    return data


def add_months_with_skills(array: list) -> dict:
    """
    This function add the overlaped time and no overlaped 
    time with the skill.
    """
    overlaped_skills = check_overlaped_skills(array)
    skills_object = create_skill_object(array)
    for item in array:
        for skill in item['skills']:
            if skill['name'] in overlaped_skills and 'overlaped_months' in item.keys():
                name = skill['name']
                skills_object[0][name].append(-item['overlaped_months'])
            if skill['name'] in skills_object[0].keys():
                name = skill['name']
                skills_object[0][name].append(item['total_months'])
    return skills_object


def calculate_skill_total_month(skills_object: dict) -> dict:
    """
    This function calculates the total month of experiences
    with overlaped time.
    """
    skills_object = skills_object
    for key, value in skills_object[0].items():
        total = sum(value)
        skills_object[0][key] = total
    return skills_object


def generate_computed_skills(data: list, skill_object: dict) -> list:
    """This function computes the skill with new information."""
    computeded_skills = []
    total_months = calculate_skill_total_month(skill_object)
    for expe in data:
        for c in expe['skills']:
            if c not in computeded_skills:
                computeded_skills.append(c)
    for skill in computeded_skills:
        if skill['name'] in total_months[0].keys():
            name = skill['name']
            total = total_months[0][name]
            skill['durationInMonths'] = total
    return computeded_skills


def send_data(professionalExperices: list) -> list:
    """This function send all the clean and ready data. """
    professional_experiences = professionalExperices
    new_data_structure = create_data_structure(professional_experiences)
    data_months_overlaped = check_month_overlaped(new_data_structure)
    skill_object = add_months_with_skills(data_months_overlaped)
    return  generate_computed_skills(professional_experiences, skill_object)
