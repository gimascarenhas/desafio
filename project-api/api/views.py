from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status

from api.utils import send_data


class FreelancerViewSet(viewsets.ViewSet):
    def create(self, request):
        try:
            freelancer_professional_experiences = request.data["freelance"]["professionalExperiences"]
            freela_id = request.data["freelance"]['id']
            computed_skills = send_data(freelancer_professional_experiences)
            data = {
                "freelancer": {
                    "id": freela_id,
                    "computedSkills": computed_skills,
                }
            }
            return Response(data)
        except:
            return Response(status=422)
