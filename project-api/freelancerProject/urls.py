from rest_framework import routers
from django.contrib import admin
from django.urls import path, include

from api.views import FreelancerViewSet

router = routers.DefaultRouter()
router.register(r'freelancer', FreelancerViewSet, basename='infoapi')

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
]
